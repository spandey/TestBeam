#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/program_options.hpp>

#include <HGCal/CondObjects/interface/HGCalCondObjects.h>
#include <HGCal/DataFormats/interface/HGCalTBDetId.h>
#include <HGCal/Geometry/interface/HGCalTBGeometryParameters.h>


int main(int argc,char**argv)
{
  std::string m_hgpedname,m_lgpedname,m_dbname;
  try { 
    namespace po = boost::program_options; 
    po::options_description desc("Options"); 
    desc.add_options() 
      ("help,h", "Print help messages") 
      ("HighGainPedestalFileName", po::value<std::string>(&m_hgpedname)->default_value("hgped.txt"), "name of input high gain pedestal file")
      ("LowGainPedestalFileName", po::value<std::string>(&m_lgpedname)->default_value("lgped.txt"), "name of input low gain pedestal file")
      ("DBName", po::value<std::string>(&m_dbname)->default_value("myDB.db"), "name of data base");
      
    po::variables_map vm; 
    try { 
      po::store(po::parse_command_line(argc, argv, desc),  vm); 
      if ( vm.count("help")  ) { 
        std::cout << desc << std::endl; 
        return 0; 
      } 
      po::notify(vm);
    }
    catch(po::error& e) { 
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      std::cerr << desc << std::endl; 
      return 1; 
    }
    if( vm.count("DBName") ) std::cout << "DBName = " << m_dbname << std::endl;
    if( vm.count("HighGainPedestalFileName") ) std::cout << "HighGainPedestalFileName = " << m_hgpedname << std::endl;
    if( vm.count("LowGainPedestalFileName") ) std::cout << "LowGainPedestalFileName = " << m_lgpedname << std::endl;
    
  }
  catch(std::exception& e) { 
    std::cerr << "Unhandled Exception reached the top of main: " 
              << e.what() << ", application will now exit" << std::endl; 
    return 2; 
  } 
  std::vector<float> pedestalHG(NUMBER_OF_SCA,0);
  std::vector<float> pedestalLG(NUMBER_OF_SCA,0);
  std::vector<float> noiseHG(NUMBER_OF_SCA,0);
  std::vector<float> noiseLG(NUMBER_OF_SCA,0);

  HGCalTBCondDB db;
  std::ifstream ifs(m_dbname);
  boost::archive::text_iarchive ia(ifs);
  ia >> db;

  FILE* file;
  char buffer[300];
  file = fopen (m_hgpedname.c_str() , "r");
  if (file == NULL){ perror ("Error opening pedestal high gain"); exit(1); }
  else{
    while ( ! feof (file) ){
      if ( fgets (buffer , 300 , file) == NULL ) break;
      const char* index = buffer;
      int hexaboard,skiroc,channel,ptr,nval;
      uint32_t elecid;
      nval=sscanf( index, "%d %d %d %n",&hexaboard,&skiroc,&channel,&ptr );
      if( nval==3 ){
  	int skiId=HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA*hexaboard+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-skiroc)%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+1;
  	elecid = (skiId<<7) | (channel&0x7f);
  	index+=ptr;
      }else continue;
      for( unsigned int ii=0; ii<NUMBER_OF_SCA; ii++ ){
  	float mean,rms;
  	nval = sscanf( index, "%f %f %n",&mean,&rms,&ptr );
  	if( nval==2 ){
  	  pedestalHG[ii]=mean;
  	  noiseHG[ii]=rms;
  	  index+=ptr;
  	}else continue;
      }
      HGCalTBCondChannel h=db.getByElecID(elecid);
      h.updatePedestalHG(pedestalHG);
      h.updateNoiseHG(noiseHG);
      db.update(h);
    }
    fclose (file);
  }

  file = fopen (m_lgpedname.c_str() , "r");
  if (file == NULL){ perror ("Error opening pedestal low gain"); exit(1); }
  else{
    while ( ! feof (file) ){
      if ( fgets (buffer , 300 , file) == NULL ) break;
      const char* index = buffer;
      int hexaboard,skiroc,channel,ptr,nval;
      uint32_t elecid;
      nval=sscanf( index, "%d %d %d %n",&hexaboard,&skiroc,&channel,&ptr );
      if( nval==3 ){
  	int skiId=HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA*hexaboard+(HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA-skiroc)%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA+1;
  	elecid = (skiId<<7) | (channel&0x7f);
	index+=ptr;
      }else continue;
      for( unsigned int ii=0; ii<NUMBER_OF_SCA; ii++ ){
  	float mean,rms;
  	nval = sscanf( index, "%f %f %n",&mean,&rms,&ptr );
  	if( nval==2 ){
  	  pedestalLG[ii]=mean;
  	  noiseLG[ii]=rms;
  	  index+=ptr;
  	}else continue;
      }
      HGCalTBCondChannel h=db.getByElecID(elecid);
      h.updatePedestalLG(pedestalLG);
      h.updateNoiseLG(noiseLG);
      db.update(h);
    }
    fclose (file);
  }

  std::ofstream ofs(m_dbname);
  boost::archive::text_oarchive oa(ofs);
  oa << db;
  ofs.close();

  int index=0;
  for( std::vector<HGCalTBCondChannel>::iterator it=db.get().begin(); it!=db.get().end(); ++it ){
    if(index<1000){
      std::cout << (*it) << std::endl;
    }
    index++;
  }

  return 0;
}
