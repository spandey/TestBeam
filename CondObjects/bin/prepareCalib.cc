#include <iostream>
#include <fstream>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/program_options.hpp>

#include <HGCal/CondObjects/interface/HGCalCondObjects.h>
#include <HGCal/DataFormats/interface/HGCalTBDetId.h>

std::map<uint32_t,uint32_t> loadEmap(std::string filename)
{
  std::map<uint32_t,uint32_t> detid_eid_map;
  FILE* f = fopen(filename.c_str(), "r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", filename.c_str());
    return detid_eid_map;
  }
  
  char buffer[100];
  while (!feof(f)) {
    buffer[0] = 0;
    fgets(buffer, 100, f);
    char* p_comment = index(buffer, '#');
    if (p_comment != 0) *p_comment = 0;
    int ptr;
    int iskiroc, icell, layer, sensorIU, sensorIV, iu, iv, cellType;
    const char* process = buffer;
    int found = sscanf(buffer, "%d %d %d %d %d %d %d %d %n ", &iskiroc, &icell, &layer, &sensorIU, &sensorIV, &iu, &iv, &cellType, &ptr);
    if (found == 8) {
      process += ptr;
    } else continue;

    uint32_t detid=0;
    detid |= ((iu   & (HGCalTBDetId::kHGCalTBXSignMask | HGCalTBDetId::kHGCalTBXMask))        << HGCalTBDetId::kHGCalTBXOffset);
    detid |= ((iv   & (HGCalTBDetId::kHGCalTBVSignMask | HGCalTBDetId::kHGCalTBVMask))        << HGCalTBDetId::kHGCalTBVOffset);
    detid |= ((sensorIU  & (HGCalTBDetId::kHGCalTBSensorXSignMask | HGCalTBDetId::kHGCalTBSensorXMask))      << HGCalTBDetId::kHGCalTBSensorXOffset);
    detid |= ((sensorIV  & (HGCalTBDetId::kHGCalTBSensorVSignMask | HGCalTBDetId::kHGCalTBSensorVMask))      << HGCalTBDetId::kHGCalTBSensorVOffset);
    detid |= ((layer    & HGCalTBDetId::kHGCalLayerMask)       << HGCalTBDetId::kHGCalLayerOffset);
    detid |= ((cellType & HGCalTBDetId::kHGCalTBCellTypeMask) << HGCalTBDetId::kHGCalTBCellTypeOffset);

    uint32_t elecid= (iskiroc<<7) | (icell&0x7f);
    std::pair<uint32_t,uint32_t> p(detid,elecid);
    if( detid_eid_map.find(detid)==detid_eid_map.end() ) 
      detid_eid_map.insert(p);
    else{
      fprintf(stderr, "MAJOR ISSUE when building detid/elecid map: detid : %d found more than once -> exit(1)", detid);
      exit(1);
    }
  }
  fclose(f);
  return detid_eid_map;
  
}

struct layoutObj{
  int moduleId;
  float z;
};
std::map<int,layoutObj> loadLayout(const std::string& filename)
{
  std::map<int,layoutObj> layoutmap;
  FILE* f = fopen(filename.c_str(),"r");
  if (f == 0) {
    fprintf(stderr, "Unable to open '%s'\n", filename.c_str());
    return layoutmap;
  }
  int layerId,x,y,moduleId;
  float z;
  char det[10];
  char buffer[100];
  int moduleIndex=0;
  while(!feof(f)){
    buffer[0]=0;
    fgets(buffer,100,f);
    char* p_comment = index(buffer, '#');
    if (p_comment != 0) continue;
    int ptr=0;
    const char* process = buffer;
    int found = sscanf(process, "%d %f %s %d %d %d %n", &layerId, &z, det, &x, &y, &moduleId, &ptr);
    if (found == 6) {
      process += ptr;
      layoutObj obj; 
      obj.moduleId=moduleId;
      obj.z=z;
      layoutmap.insert( std::pair<int,layoutObj>(moduleIndex,obj) );
      moduleIndex++;
    } else continue;
  }
  fclose(f);
  return layoutmap;
}

int main(int argc,char**argv)
{
  std::string m_emapname,m_layoutname,m_dbname;
  try { 
    namespace po = boost::program_options; 
    po::options_description desc("Options"); 
    desc.add_options() 
      ("help,h", "Print help messages") 
      ("emapName", po::value<std::string>(&m_emapname)->default_value("emap.txt"), "name of input electronic map file")
      ("layoutName", po::value<std::string>(&m_layoutname)->default_value("layout.txt"), "name of input geometry layout file")
      ("DBName", po::value<std::string>(&m_dbname)->default_value("myDB.db"), "name of output data base");
  
    po::variables_map vm; 
    try { 
      po::store(po::parse_command_line(argc, argv, desc),  vm); 
      if ( vm.count("help")  ) { 
        std::cout << desc << std::endl; 
        return 0; 
      } 
      po::notify(vm);
    }
    catch(po::error& e) { 
      std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
      std::cerr << desc << std::endl; 
      return 1; 
    }
    if( vm.count("emapName") ) std::cout << "emapName = " << m_emapname << std::endl;
    if( vm.count("layoutName") ) std::cout << "layoutName = " << m_layoutname << std::endl;
    if( vm.count("DBName") ) std::cout << "DBName = " << m_dbname << std::endl;
  
  }
  catch(std::exception& e) { 
    std::cerr << "Unhandled Exception reached the top of main: " 
              << e.what() << ", application will now exit" << std::endl; 
    return 2; 
  } 
  std::vector<float> pedestalHG(NUMBER_OF_SCA,220);
  std::vector<float> pedestalLG(NUMBER_OF_SCA,210);
  std::vector<float> noiseHG(NUMBER_OF_SCA,10);
  std::vector<float> noiseLG(NUMBER_OF_SCA,1);

  HGCalTBCondDB db;

  std::map<uint32_t,uint32_t> detid_eid_map=loadEmap(m_emapname);  
  std::map<int,layoutObj> layoutmap=loadLayout(m_layoutname);

  for(std::map<uint32_t,uint32_t>::iterator it=detid_eid_map.begin(); it!=detid_eid_map.end(); ++it ){
    HGCalTBCondChannel h(it->first,it->second);
    int skiId=it->second>>7;
    int boardIndex=(skiId-1)/4;
    int moduleId=layoutmap[boardIndex].moduleId;
    float z=layoutmap[boardIndex].z;
    h.setModule(moduleId);
    h.setZ(z);
    h.updatePedestalHG(pedestalHG);
    h.updatePedestalLG(pedestalLG);
    h.updateNoiseHG(noiseHG);
    h.updateNoiseLG(noiseLG);
    db.add(h);
  }
  
  std::ofstream ofs(m_dbname);
  boost::archive::text_oarchive oa(ofs);
  oa << db;
  ofs.close();

  std::cout << "\n\n";
  HGCalTBCondDB dbnew;
  std::ifstream ifs(m_dbname);
  boost::archive::text_iarchive ia(ifs);
  ia >> dbnew;
  int index=0;
  for(std::map<uint32_t,uint32_t>::iterator it=detid_eid_map.begin(); it!=detid_eid_map.end(); ++it ){
    HGCalTBCondChannel h=dbnew.getByDetID(it->first);
    if(index<1000){
      std::cout << h << std::endl;
    }
    index++;
  }

  std::cout << "Size of DB = " << dbnew.get().size() << std::endl;

  return 0;
}
