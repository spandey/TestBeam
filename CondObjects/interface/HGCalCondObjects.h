#ifndef HGCAL_CONDOBJECTS
#define HGCAL_CONDOBJECTS 1

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/serialization/vector.hpp>

#define NUMBER_OF_SCA 13

enum CalibrationFlag
{
  FullyCalibrated,
  PartiallyCalibrated,
  NotCalibrated
};
class HGCalTBCondChannel
{
public:

  HGCalTBCondChannel()
  {;}
  HGCalTBCondChannel(uint32_t detid)
  {
    m_detid=detid;
    m_elecid=0;
    m_module=0;
    m_z=0;
    m_pedestalHG.reserve(NUMBER_OF_SCA);
    m_pedestalLG.reserve(NUMBER_OF_SCA);
    m_noiseHG.reserve(NUMBER_OF_SCA);
    m_noiseLG.reserve(NUMBER_OF_SCA);
    m_calibrationFlag=NotCalibrated;
  }
  HGCalTBCondChannel(uint32_t detid, uint32_t elecid)
  {
    m_detid=detid;
    m_elecid=elecid;
    m_module=0;
    m_z=0;
    m_pedestalHG.reserve(NUMBER_OF_SCA);
    m_pedestalLG.reserve(NUMBER_OF_SCA);
    m_noiseHG.reserve(NUMBER_OF_SCA);
    m_noiseLG.reserve(NUMBER_OF_SCA);
    m_calibrationFlag=NotCalibrated;
  }
  HGCalTBCondChannel(uint32_t detid, uint32_t elecid, uint16_t module)
  {
    m_detid=detid;
    m_elecid=elecid;
    m_module=module;
    m_pedestalHG.reserve(NUMBER_OF_SCA);
    m_pedestalLG.reserve(NUMBER_OF_SCA);
    m_noiseHG.reserve(NUMBER_OF_SCA);
    m_noiseLG.reserve(NUMBER_OF_SCA);
    m_calibrationFlag=NotCalibrated;
  }
  ~HGCalTBCondChannel()
  {
    m_pedestalHG.clear();
    m_pedestalLG.clear();
    m_noiseHG.clear();
    m_noiseLG.clear();
  }

  inline const uint32_t detid() const{return m_detid;}
  inline const uint32_t elecid() const{return m_elecid;}
  inline const uint16_t module() const{return m_module;}
  inline const float z() const{return m_z;}
  inline const std::vector<float> &pedestalHG() {return m_pedestalHG;}
  inline const std::vector<float> &pedestalLG() {return m_pedestalLG;}
  inline const std::vector<float> &noiseHG() {return m_noiseHG;}
  inline const std::vector<float> &noiseLG() {return m_noiseLG;}
  inline const float highGainMIP() {return m_hgMIP;}
  inline const float highGainSAT() {return m_hgSAT;}
  inline const float lowGainMIP() {return m_lgMIP;}
  inline const float lowGainSAT() {return m_lgSAT;}
  inline const float totCOEF() {return m_totCOEF;}
  inline const float totTHR() {return m_totTHR;}
  inline const float totPED() {return m_totPED;}
  inline const float totNORM() {return m_totNORM;}
  inline const float totPOW() {return m_totPOW;}
  inline const CalibrationFlag calibrationFlag() {return m_calibrationFlag;}
  
  inline void setDetid(uint32_t id) {m_detid=id;}
  inline void setElecid(uint32_t id) {m_elecid=id;}
  inline void setModule(uint16_t module) {m_module=module;}
  inline void setZ(float z) {m_z=z;}

  inline void updatePedestalHG(std::vector<float> vec) {m_pedestalHG=vec;}
  inline void updatePedestalLG(std::vector<float> vec) {m_pedestalLG=vec;}
  inline void updateNoiseHG(std::vector<float> vec) {m_noiseHG=vec;}
  inline void updateNoiseLG(std::vector<float> vec) {m_noiseLG=vec;}
  inline void updateHighGainMIP(float val) {m_hgMIP=val;}
  inline void updateHighGainSAT(float val) {m_hgSAT=val;}
  inline void updateLowGainMIP(float val) {m_lgMIP=val;}
  inline void updateLowGainSAT(float val) {m_lgSAT=val;}
  inline void updateTotCOEF(float val) {m_totCOEF=val;}
  inline void updateTotTHR(float val) {m_totTHR=val;}
  inline void updateTotPED(float val) {m_totPED=val;}
  inline void updateTotNORM(float val) {m_totNORM=val;}
  inline void updateTotPOW(float val) {m_totPOW=val;}
  inline void updateCalibFlag(CalibrationFlag flag) {m_calibrationFlag=flag;}

  bool operator==(const HGCalTBCondChannel& h)
  {
    return m_detid==h.detid();
  }

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & m_detid;
    ar & m_elecid;
    ar & m_module;
    ar & m_z;
    ar & m_pedestalHG;
    ar & m_pedestalLG;
    ar & m_noiseHG;
    ar & m_noiseLG;

    ar & m_hgMIP;
    ar & m_hgSAT;
    ar & m_lgMIP;
    ar & m_lgSAT;
    ar & m_totCOEF;
    ar & m_totTHR;
    ar & m_totPED;
    ar & m_totNORM;
    ar & m_totPOW;
    ar & m_calibrationFlag;
  }
  
  uint32_t m_detid;
  uint32_t m_elecid;
  uint16_t m_module;
  float m_z;
  std::vector<float> m_pedestalHG, m_pedestalLG, m_noiseHG, m_noiseLG;
  float m_hgMIP, m_hgSAT;
  float m_lgMIP, m_lgSAT;
  float m_totCOEF, m_totTHR, m_totPED, m_totNORM, m_totPOW;
  CalibrationFlag m_calibrationFlag;
};

std::ostream& operator<<(std::ostream& out,HGCalTBCondChannel h)
{
  int skirocID=(h.elecid()>>7)&0x1ff;
  int channelID=h.elecid()&0x7f;
  out << std::setprecision(16) << h.detid() << " " << std::setprecision(4) << skirocID << " " << channelID << '\t' << h.module() << "\t" << h.z() << "\t";
  out << std::setprecision(3) ;
  for(int i=0; i<NUMBER_OF_SCA; i++)
    out << " " << h.pedestalHG()[i] ;
  out << " ";
  for(int i=0; i<NUMBER_OF_SCA; i++)
    out << " " << h.noiseHG()[i] ;
  out << "\t\t";
  for(int i=0; i<NUMBER_OF_SCA; i++)
    out << " " << h.pedestalLG()[i] ;
  out << " ";
  for(int i=0; i<NUMBER_OF_SCA; i++)
    out << " " << h.noiseLG()[i] ;
  out << "\t";
  out << " " << h.highGainMIP();
  out << " " << h.highGainSAT();
  out << " " << h.lowGainMIP();
  out << " " << h.lowGainSAT();
  out << " " << h.totCOEF();
  out << " " << h.totTHR();
  out << " " << h.totPED();
  out << " " << h.totNORM();
  out << " " << h.totPOW();
  return out;
}


class HGCalTBCondDB{

public:

  HGCalTBCondDB(){;}
  ~HGCalTBCondDB()
  {
    m_channels.clear();
  }
  
  void add(HGCalTBCondChannel &h)
  {
    m_channels.push_back(h);
  }

  void update(HGCalTBCondChannel &h)
  {
    std::vector<HGCalTBCondChannel>::iterator it=std::find(m_channels.begin(), m_channels.end(),h);
    if( it==m_channels.end() )
      std::cout << "Cond channel " << h << "was not found in condition DB" << std::endl;
    else 
      (*it)=h;
  }

  void update(std::vector<HGCalTBCondChannel> &vec)
  {
    for( std::vector<HGCalTBCondChannel>::iterator it=vec.begin(); it!=vec.end(); ++it ){
      std::vector<HGCalTBCondChannel>::iterator jt=std::find(m_channels.begin(), m_channels.end(),(*it));
      if( jt==m_channels.end() )
	std::cout << "Cond channel " << (*it) << "was not found in condition DB" << std::endl;
      else 
	(*jt)=(*it);
    }
  }

  std::vector<HGCalTBCondChannel>& get()
  {
    return m_channels;
  }

  HGCalTBCondChannel getByDetID(uint32_t detid)
  {
    HGCalTBCondChannel h(detid);
    std::vector<HGCalTBCondChannel>::iterator it=std::find(m_channels.begin(), m_channels.end(),h);
    if( it==m_channels.end() )
      std::cout << "the detid " << detid << " could not be found in the HGCalTBCondDB -> it might be an issue" << std::endl; 
    return (*it);
  }
  HGCalTBCondChannel getByElecID(uint32_t id)
  {
    std::vector<HGCalTBCondChannel>::iterator return_iter=m_channels.end();
    for( std::vector<HGCalTBCondChannel>::iterator it=m_channels.begin(); it!=m_channels.end(); ++it ){
      if( (*it).elecid()==id )
	return_iter=it;
    }
    if( return_iter==m_channels.end() )
      std::cout << "the elecid " << id << " could not be found in the HGCalTBCondDB -> it might be an issue" << std::endl; 
    return (*return_iter);
  }

private:
  friend class boost::serialization::access;
  template<class Archive>
  void serialize(Archive & ar, const unsigned int version)
  {
    ar & m_channels;
  }

  std::vector<HGCalTBCondChannel> m_channels;

};

#endif 
