#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <set>
#include <cmath>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"

#include "HGCal/DataFormats/interface/HGCalTBRawHitCollection.h"
#include "HGCal/DataFormats/interface/HGCalTBDetId.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "HGCal/CondObjects/interface/HGCalElectronicsMap.h"
#include "HGCal/CondObjects/interface/HGCalCondObjectTextIO.h"
#include "HGCal/DataFormats/interface/HGCalTBElectronicsId.h"
#include "HGCal/CondObjects/interface/HGCalTBDetectorLayout.h"
#include "HGCal/DataFormats/interface/HGCalTBLayer.h"
#include "HGCal/DataFormats/interface/HGCalTBModule.h"

#include "TTree.h"

class AutoCorrelationPlotter : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
public:
  explicit AutoCorrelationPlotter(const edm::ParameterSet&);
  ~AutoCorrelationPlotter();
  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);
private:
  virtual void beginJob() override;
  void analyze(const edm::Event& , const edm::EventSetup&) override;
  virtual void endJob() override;
  void createAutoCorrelationHistograms(int module, int board);
  
  std::string m_electronicMap;
  std::string m_detectorLayoutFile;
  HGCalElectronicsMap m_emap;
  HGCalTBDetectorLayout m_layout;

  int m_evtID;
  uint16_t m_numberOfBoards;

  edm::EDGetTokenT<HGCalTBRawHitCollection> m_HGCalTBRawHitCollection;

  struct hgcal_channel{
    hgcal_channel() : key(0),
		      module(0){
      for(int t=0;t<NUMBER_OF_TIME_SAMPLES;t++){
	meanHG[t]=0.;
	meanLG[t]=0.;
	rmsHG[t]=0.;
	rmsLG[t]=0.;
      }
    }
    int key;
    int module;
    float meanHG[NUMBER_OF_TIME_SAMPLES];
    float meanLG[NUMBER_OF_TIME_SAMPLES];
    float rmsHG[NUMBER_OF_TIME_SAMPLES];
    float rmsLG[NUMBER_OF_TIME_SAMPLES];
    std::vector<float> highGain[NUMBER_OF_TIME_SAMPLES];
    std::vector<float> lowGain[NUMBER_OF_TIME_SAMPLES];
  };
  std::map<int,hgcal_channel> m_channelMap;

};

AutoCorrelationPlotter::AutoCorrelationPlotter(const edm::ParameterSet& iConfig) :
  m_electronicMap(iConfig.getUntrackedParameter<std::string>("ElectronicMap","HGCal/CondObjects/data/emap.txt")),
  m_detectorLayoutFile(iConfig.getUntrackedParameter<std::string>("DetectorLayout","HGCal/CondObjects/data/layerGeom.txt"))
{
  m_HGCalTBRawHitCollection = consumes<HGCalTBRawHitCollection>(iConfig.getParameter<edm::InputTag>("InputCollection"));
  m_evtID=0;
  std::cout << iConfig.dump() << std::endl;
}


AutoCorrelationPlotter::~AutoCorrelationPlotter()
{

}

void AutoCorrelationPlotter::beginJob()
{
  HGCalCondObjectTextIO io(0);
  edm::FileInPath fip(m_electronicMap);
  if (!io.load(fip.fullPath(), m_emap)) {
    throw cms::Exception("Unable to load electronics map");
  };
  fip=edm::FileInPath(m_detectorLayoutFile);
  if (!io.load(fip.fullPath(), m_layout)) {
    throw cms::Exception("Unable to load detector layout file");
  };
  for( auto layer : m_layout.layers() )
    layer.print();
}

void AutoCorrelationPlotter::analyze(const edm::Event& event, const edm::EventSetup& setup)
{
  edm::Handle<HGCalTBRawHitCollection> hits;
  event.getByToken(m_HGCalTBRawHitCollection, hits);

  m_evtID++;
  if( !hits->size() ) return;
  
  for( auto hit : *hits ){
    HGCalTBElectronicsId eid( m_emap.detId2eid(hit.detid().rawId()) );
    if( !m_emap.existsEId(eid) ) continue;
    
    int iskiroc=hit.skiroc();//from 0 to numberOfHexaboard*4-1
    int iboard=iskiroc/HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;
    int ichip=iskiroc%HGCAL_TB_GEOMETRY::N_SKIROC_PER_HEXA;//from 0 to 3
    uint32_t key=iboard*1000+ichip*100+hit.channel();
    std::map<int,hgcal_channel>::iterator iter=m_channelMap.find(key);
    if( iter==m_channelMap.end() ){
      hgcal_channel tmp;
      tmp.key=key;

      HGCalTBLayer alayer = m_layout.at( hit.detid().layer()-1 );
      HGCalTBModule amodule = alayer.at( hit.detid().sensorIU(), hit.detid().sensorIV() );
      tmp.module=amodule.moduleID();

      std::vector<float> vecH[NUMBER_OF_TIME_SAMPLES],vecL[NUMBER_OF_TIME_SAMPLES];
      for(int it=0; it<NUMBER_OF_TIME_SAMPLES; it++){
	vecH[it].push_back(hit.highGainADC(it));
	vecL[it].push_back(hit.lowGainADC(it));
	tmp.highGain[it]=vecH[it];
	tmp.lowGain[it]=vecL[it];
      }
      std::pair<int,hgcal_channel> p(key,tmp);
      m_channelMap.insert( p );
    }
    else{
      for(int it=0; it<NUMBER_OF_TIME_SAMPLES; it++){
	iter->second.highGain[it].push_back(hit.highGainADC(it));
	iter->second.lowGain[it].push_back(hit.lowGainADC(it));
      }
    }
  }
}

void AutoCorrelationPlotter::endJob()
{
  TTree* tree;  
  int channel;
  int chip;
  int board;
  int module;
  std::vector<float> corrHG;
  std::vector<float> corrLG;
  usesResource("TFileService");
  edm::Service<TFileService> fs;
  tree=fs->make<TTree>("autocorrelation","Tree with autocorrelation values");
  tree->Branch("board",&board);
  tree->Branch("chip",&chip);
  tree->Branch("channel",&channel);
  tree->Branch("module",&module);
  tree->Branch("autocorr_hg",&corrHG);
  tree->Branch("autocorr_lg",&corrLG);


  for( std::map<int,hgcal_channel>::iterator it=m_channelMap.begin(); it!=m_channelMap.end(); ++it ){
    for(int t=0;t<NUMBER_OF_TIME_SAMPLES; t++){
      it->second.meanHG[t] = 0.;
      it->second.meanLG[t] = 0.;
      it->second.rmsHG[t] = 0.;
      it->second.rmsLG[t] = 0.;
      unsigned int size = it->second.highGain[t].size();
      for( unsigned int ihit=0; ihit<size; ihit++ ){
	it->second.meanHG[t]+=it->second.highGain[t].at(ihit);
	it->second.meanLG[t]+=it->second.lowGain[t].at(ihit);
	it->second.rmsHG[t]+=it->second.highGain[t].at(ihit)*it->second.highGain[t].at(ihit);
	it->second.rmsLG[t]+=it->second.lowGain[t].at(ihit)*it->second.lowGain[t].at(ihit);
      }
      it->second.meanHG[t]/=size;
      it->second.meanLG[t]/=size;
      it->second.rmsHG[t]=std::sqrt( it->second.rmsHG[t]/size-it->second.meanHG[t]*it->second.meanHG[t] );
      it->second.rmsLG[t]=std::sqrt( it->second.rmsLG[t]/size-it->second.meanLG[t]*it->second.meanLG[t] );
    }
  }

  std::map<int,hgcal_channel>::iterator it=m_channelMap.begin();
  while( it!=m_channelMap.end() ){
    board=it->first/1000;
    chip=(it->first%1000)/100;
    channel=it->first%100;
    module=it->second.module;
    corrHG.clear();
    corrLG.clear();
    for(int t=0; t<NUMBER_OF_TIME_SAMPLES; t++){
      float meanhghg(0.),meanlglg(0.);
      for( unsigned int ievt=0; ievt<it->second.highGain[t].size(); ievt++ ){
	meanhghg+=it->second.highGain[0].at(ievt)*it->second.highGain[t].at(ievt);
	meanlglg+=it->second.lowGain[0].at(ievt)*it->second.lowGain[t].at(ievt);
      }
      meanhghg/=it->second.highGain[t].size();
      meanlglg/=it->second.lowGain[t].size();
      corrHG.push_back( (meanhghg-it->second.meanHG[0]*it->second.meanHG[t])/(it->second.rmsHG[0]*it->second.rmsHG[t]) );
      corrLG.push_back( (meanlglg-it->second.meanLG[0]*it->second.meanLG[t])/(it->second.rmsLG[0]*it->second.rmsLG[t]) );
    }
    tree->Fill();
    m_channelMap.erase(it);
    it=m_channelMap.begin();
  }
}

void AutoCorrelationPlotter::fillDescriptions(edm::ConfigurationDescriptions& descriptions)
{
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

DEFINE_FWK_MODULE(AutoCorrelationPlotter);
