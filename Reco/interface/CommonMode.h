#ifndef HGCAL_COMMONMODE
#define HGCAL_COMMONMODE

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "HGCal/DataFormats/interface/HGCalTBRawHitCollection.h"
#include "HGCal/DataFormats/interface/HGCalTBDetId.h"
#include "HGCal/DataFormats/interface/HGCalTBCommonModeNoise.h"
#include "HGCal/Geometry/interface/HGCalTBCellVertices.h"
#include "HGCal/CondObjects/interface/HGCalElectronicsMap.h"
#include "HGCal/DataFormats/interface/HGCalTBElectronicsId.h"

enum CommonModeNoiseMethod{
  MEDIANPERCHIP,
  MEDIANPERBOARD,
  MEDIANPERBOARDWITHTHRESHOLD,
  NOCMSUBTRACTION //If this option is used, the CommonModeNoiseMap has 1 entry per CHIP, commonModeNoise values are then equal to 0
};
  
class CommonMode{
 public:
  CommonMode(){;}
  CommonMode( HGCalElectronicsMap &map, CommonModeNoiseMethod meth );
  ~CommonMode(){}
  void Evaluate( edm::Handle<HGCalTBRawHitCollection> hits );
  std::map<int,commonModeNoise> CommonModeNoiseMap() const {return m_cmMap;} //Be careful: the size of the returned map will depend on the value of the CommonModeNoiseMethod option
  inline void setHighGainThreshold(int thr=100){ m_threshold=thr;} //needed when using MEDIANPERBOARDWITHTHRESHOLD option
  inline void setExpectedTMax(int tmax=3){ m_expectedMaxTS=tmax;} //needed when using MEDIANPERBOARDWITHTHRESHOLD option
 private:
  void InitPerCHIP(); //memory allocation is done here
  void InitPerBoard(); //or here
  void ResetPerCHIP();
  void ResetPerBoard();  
  void EvaluateMedianPerChip( edm::Handle<HGCalTBRawHitCollection> hits );
  void EvaluateMedianPerBoard( edm::Handle<HGCalTBRawHitCollection> hits );
  void EvaluateAveragePerChip( edm::Handle<HGCalTBRawHitCollection> hits );
  void EvaluateMedianPerBoardWithThr( edm::Handle<HGCalTBRawHitCollection> hits );


  HGCalElectronicsMap m_emap;

  float m_threshold;
  int m_expectedMaxTS;
  CommonModeNoiseMethod m_cmeth;

  std::map<int,commonModeNoise> m_cmMap;
  std::vector<float> *p_cmMapHG;
  std::vector<float> *p_cmMapLG;
  std::vector<float> *p_cmMapHGHalf;
  std::vector<float> *p_cmMapLGHalf;
  
};

#endif
