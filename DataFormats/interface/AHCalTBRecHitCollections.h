#ifndef DATAFORMATS_AHCRECHIT_AHCRECHITCOLLECTION_H
#define DATAFORMATS_AHCRECHIT_AHCRECHITCOLLECTION_H

#include "DataFormats/Common/interface/EDCollection.h"
#include "DataFormats/Common/interface/SortedCollection.h"
#include "HGCal/DataFormats/interface/AHCalTBRecHit.h"
#include "DataFormats/Common/interface/Ref.h"
#include "DataFormats/Common/interface/RefVector.h"


typedef edm::SortedCollection<AHCalTBRecHit> AHCalTBRecHitCollection;
typedef edm::Ref<AHCalTBRecHitCollection> AHCalTBRecHitRef;
typedef edm::RefVector<AHCalTBRecHitCollection> AHCalTBRecHitRefs;
typedef edm::RefProd<AHCalTBRecHitCollection> AHCalTBRecHitsRef;

#endif
